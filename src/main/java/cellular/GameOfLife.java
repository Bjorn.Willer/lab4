package cellular;

import java.util.Random;



import datastructure.CellGrid;
import datastructure.IGrid;

/**
 * 
 * A CellAutomata that implements Conways game of life.
 * 
 * @see CellAutomaton
 * 
 *      Every cell has two states: Alive or Dead. Each step the state of each
 *      cell is decided from its neighbors (diagonal, horizontal and lateral).
 *      If the cell has less than two alive Neighbors or more than three
 *      neighbors the cell dies. If a dead cell has exactly three neighbors it
 *      will become alive.
 * 
 * @author eivind
 * @author Martin Vatshelle - martin.vatshelle@uib.no
 * @author Sondre Bolland - sondre.bolland@uib.no
 */
public class GameOfLife implements CellAutomaton {

	/**
	 * The grid of cells
	 */
	IGrid currentGeneration;

	/**
	 * 
	 * Construct a Game Of Life Cell Automaton that holds cells in a grid of the
	 * provided size
	 * 
	 * @param height The height of the grid of cells
	 * @param width  The width of the grid of cells
	 */
	public GameOfLife(int rows, int columns) {
		this.currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
	}

	@Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < this.currentGeneration.numRows(); row++) {
			for (int col = 0; col < this.currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					this.currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					this.currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}

	@Override
	public int numberOfRows() {
		
		return this.currentGeneration.numRows();
	}

	@Override
	public int numberOfColumns() {
		
		return this.currentGeneration.numColumns();
	}

	@Override
	public CellState getCellState(int row, int col) {
		
		return this.currentGeneration.get(row, col);
	}

	@Override
	public void step() {
		IGrid nextGeneration = this.currentGeneration.copy();
		for (int r=0; r<numberOfRows(); r++){
			for (int c=0; c<numberOfColumns(); c++){
				nextGeneration.set(r, c, getNextCell(r,c));
			}

		}
		this.currentGeneration = nextGeneration;




		
	}

	@Override
	public CellState getNextCell(int row, int col) {
		if(getCellState(row, col).equals(CellState.ALIVE)){ 

			if (countNeighbors(row, col, CellState.ALIVE)==2 || countNeighbors(row, col, CellState.ALIVE)==3){
				return CellState.ALIVE;
		}
		

	}
		if(getCellState(row, col).equals(CellState.DEAD)){
			if (countNeighbors(row, col, CellState.ALIVE)==3){
				return CellState.ALIVE;
			}
		
		}
		return CellState.DEAD;
	}
	

		
			


	/**
	 * Calculates the number of neighbors having a given CellState of a cell on
	 * position (row, col) on the board
	 * 
	 * Note that a cell has 8 neighbors in total, of which any number between 0 and
	 * 8 can be the given CellState. The exception are cells along the boarders of
	 * the board: these cells have anywhere between 3 neighbors (in the case of a
	 * corner-cell) and 5 neighbors in total.
	 * 
	 * @param x     the x-position of the cell
	 * @param y     the y-position of the cell
	 * @param state the Cellstate we want to count occurences of.
	 * @return the number of neighbors with given state
	 */
	private int countNeighbors(int row, int col, CellState state) {
			int stateNeighbors=0;

			for (int r=row-1; r<=row+1; r++){
				for (int c=col-1; c<=col+1; c++){
					if (r>=numberOfRows()||c>=numberOfColumns()||r<0||c<0||(r==row&&c==col)){
						continue;
					}
					if (this.currentGeneration.get(r,c).equals(state)){
						stateNeighbors++;

					}
				}

			}

	
		return stateNeighbors;
	}

	@Override
	public IGrid getGrid() {
		return this.currentGeneration;
	}
}
