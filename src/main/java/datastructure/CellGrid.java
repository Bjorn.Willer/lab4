package datastructure;

import java.util.ArrayList;

import cellular.CellState;
import java.util.Optional;


public class CellGrid implements IGrid {
    int rows;
    int columns;
    CellState initialState;
    ArrayList<CellState> grid;
    

    public CellGrid(int rows, int columns, CellState initialState) {
        this.rows = rows;
        this.columns = columns;

        grid = new ArrayList<CellState>();

        for(rows=0; rows<this.rows; rows++){
            for(columns=0; columns<this.columns; columns++){



                grid.add(initialState);

            }
            
        }
        
    
		

	}

    @Override
    public int numRows() {
        
        return this.rows;
    }

    @Override
    public int numColumns() {
     
        return this.columns;
    }

    @Override
    public void set(int row, int column, CellState element) {

     
       
       if (row <= this.rows && column <= this.columns &&row>=0 &&column>=0){
            this.grid.set((row*numColumns()+column), element);
        }
        

        else { throw new IndexOutOfBoundsException();
        }
        
        
    }

    @Override
    public CellState get(int row, int column) {
     

    

        

       if (row <= this.rows && column <= this.columns && row>=0 &&column>=0){
            return grid.get(row*numColumns()+column);
        }
        
      

        else { throw new IndexOutOfBoundsException();
        }
        }
       
    

    @Override
    public IGrid copy() {
        

        IGrid copyGrid = new CellGrid(this.rows, this.columns, this.initialState);

        for (int row=0; row<this.rows; row++){
            for(int column=0; column<columns; column++){

                copyGrid.set(row, column, get(row, column));

            }

        }




        
        
        return copyGrid;
    }
    
}
